#!/usr/bin/env bash

source .profile.sh

AGENTSRV_DB_PATH=./kek.sqlite pipenv run gunicorn \
    --workers=2 \
    --bind=0.0.0.0:8000 \
    --pythonpath "$(pipenv --venv)" \
    --access-logfile=- \
    lwtf.agentsrv.wsgi:app
