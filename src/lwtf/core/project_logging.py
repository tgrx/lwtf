import logging

from dynaconf import settings


def configure_logging():
    logging.basicConfig(
        format="{asctime}\t[{name}:{levelname}] {lineno:>5}|{module}.{funcName}: {message}",
        datefmt="%Y-%m-%d %H:%M:%S",
        style="{",
        level=settings.get("LOGGING_LEVEL", logging.INFO),
    )
