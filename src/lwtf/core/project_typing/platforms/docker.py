from dataclasses import dataclass
from typing import Dict, Text

from lwtf.core.consts import SUPPORTED_DOCKER_IMAGES
from lwtf.core.project_typing.buildable import IBuildable


@dataclass(frozen=True)
class DockerT(IBuildable):
    image: Text
    command: Text

    @classmethod
    def _verify_build_kwargs(cls, kw: Dict):
        image = kw["image"]

        name, *tags = image.split(":")

        if name not in SUPPORTED_DOCKER_IMAGES:
            raise ValueError(f"images `{name}:*` are not supported")

        if not tags:
            return

        if len(tags) != 1:
            raise ValueError(f"malformed tag in image `{image}`")

        tag = tags[0]
        if tag not in SUPPORTED_DOCKER_IMAGES[name]:
            raise ValueError(f"tag `{tag}` is not supported of image `{name}`")
