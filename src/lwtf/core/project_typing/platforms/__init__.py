from typing import FrozenSet, Generator, Iterable, Union

from lwtf.core.consts import SUPPORTED_PLATFORMS
from .docker import DockerT

SupportedPlatformTypesT = Union[DockerT]
PlatformsT = FrozenSet[SupportedPlatformTypesT]

_builders = {"docker": DockerT}


def list_platforms(lst: Iterable) -> Generator[SupportedPlatformTypesT, None, None]:
    for kw in lst:
        for kind, params in kw.items():
            if kind not in SUPPORTED_PLATFORMS:
                raise ValueError(f"unsupported platform `{kind}`")

            builder = _builders[kind]
            platform = builder.build(**params)
            yield platform
