from dataclasses import dataclass
from typing import Dict, Optional, Text, Tuple

from lwtf.core.consts import RE_VALUE, SUPPORTED_UNITS
from lwtf.core.project_typing.buildable import IBuildable


def parse_value(value: Text) -> Tuple[int, Text]:
    try:
        groups = RE_VALUE.match(value).groupdict()
    except AttributeError as _err:
        raise ValueError(f"malformed value `{value}`") from _err

    return int(groups["amount"]), groups["unit"]


@dataclass(frozen=True)
class ConstraintValueT(IBuildable):
    amount: int
    unit: str

    def absolute(self):
        multiplier = SUPPORTED_UNITS[self.unit]
        return self.amount * multiplier

    @classmethod
    def build(cls, value: Text) -> "ConstraintValueT":
        amount, unit = parse_value(value)
        return ConstraintValueT(amount=amount, unit=unit)


@dataclass(frozen=True)
class ConstraintsT(IBuildable):
    cpu: Optional[ConstraintValueT]
    memory: Optional[ConstraintValueT]
    timeout: Optional[ConstraintValueT]

    @classmethod
    def _build(cls, kw: Dict):
        return ConstraintsT(
            cpu=ConstraintValueT.build(kw["cpu"]),
            memory=ConstraintValueT.build(kw["memory"]),
            timeout=ConstraintValueT.build(kw["timeout"]),
        )
