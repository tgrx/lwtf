from typing import Dict


class IBuildable:
    """
    Interface for those named tuples, which can be constructed via build()

    One can override _-methods to tune building.

    A schema of API calls:

    build()  # general build flow
    |
    +- verify_build_args()  # verifies kwargs against namedtuple
    |  |
    |  +- _verify_build_kwargs()  # additional verification
    |
    +< _build()  # actual creation of object with kwargs
    """

    @classmethod
    def build(cls, **kwargs):
        cls.verify_build_kwargs(kwargs)
        return cls._build(kwargs.copy())

    @classmethod
    def verify_build_kwargs(cls, kwargs: Dict):
        keywords_expected = frozenset(getattr(cls, "__annotations__"))
        keywords_passed = frozenset(kwargs.keys())
        keywords_extra = keywords_passed - keywords_expected
        keywords_missing = keywords_expected - keywords_passed

        if any((keywords_extra, keywords_missing)):
            raise TypeError(
                f"{cls}.build() malformed kwargs:"
                f" missing={sorted(keywords_missing)},"
                f" extra={sorted(keywords_extra)}"
            )

        cls._verify_build_kwargs(kwargs)

    @classmethod
    def _build(cls, kw: Dict):
        return cls(**kw)

    @classmethod
    def _verify_build_kwargs(cls, kw: Dict):
        return
