from dataclasses import dataclass
from typing import Dict, FrozenSet, Generator, Text, Tuple

import strictyaml

from lwtf.core.consts import TESTS_DIR
from lwtf.core.project_typing import ConstraintsT, PlatformsT
from lwtf.core.project_typing.buildable import IBuildable
from lwtf.core.project_typing.platforms import list_platforms


@dataclass(frozen=True)
class TestCaseT(IBuildable):
    name: Text
    path: Text
    suites: FrozenSet[Text]
    keywords: FrozenSet[Text]
    actions: Tuple[Text]
    platforms: PlatformsT
    constraints: ConstraintsT

    @classmethod
    def _build(cls, kw: Dict):
        return TestCaseT(
            name=kw["name"],
            path=kw["path"],
            suites=frozenset(_elm for _elm in kw.get("suites", [])),
            keywords=frozenset(_elm for _elm in kw.get("keywords", [])),
            actions=tuple(str(_elm) for _elm in kw.get("actions", [])),
            platforms=frozenset(list_platforms(kw.get("platforms", []))),
            constraints=ConstraintsT.build(**kw.get("constraints", {})),
        )

    @staticmethod
    def list_tests() -> Generator["TestCaseT", None, None]:
        for path in TESTS_DIR.glob("**/*.yml"):
            with path.open(mode="r", encoding="utf-8") as fp:
                yml = fp.read()

                kw = strictyaml.load(yml).data
                kw["path"] = path

            obj = TestCaseT.build(**kw)

            yield obj
