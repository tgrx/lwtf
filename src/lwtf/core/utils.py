import subprocess
from pathlib import Path


def get_pipenv_path() -> Path:
    r = subprocess.run(args=("which", "pipenv"), capture_output=True, check=True)

    return Path(r.stdout.decode().strip()).resolve()


def get_venv_path() -> Path:
    pipenv_path = get_pipenv_path()

    r = subprocess.run(args=(pipenv_path, "--venv"), capture_output=True, check=True)

    return Path(r.stdout.decode().strip()).resolve()
