import re
from pathlib import Path

from dynaconf import settings

FRAMEWORK_DIR: Path = Path(__file__).parent.parent.resolve()
assert FRAMEWORK_DIR.is_dir()

CORE_DIR: Path = FRAMEWORK_DIR / "core"
assert CORE_DIR.is_dir()

AGENTSRV_DIR: Path = FRAMEWORK_DIR / "agentsrv"
assert AGENTSRV_DIR.is_dir()

MASTER_DIR: Path = FRAMEWORK_DIR / "master"
assert MASTER_DIR.is_dir()

TESTS_DIR: Path = Path(settings.TESTS_DIR.format(**locals())).resolve()
assert TESTS_DIR.is_dir()

SUPPORTED_DOCKER_IMAGES = {
    "ubuntu": {"latest"},
    "alpine": {"latest"},
    "python": {"3.7-alpine"},
}

SUPPORTED_PLATFORMS = frozenset({"docker"})

SUPPORTED_UNITS = {
    # cpu units
    "%": 1,
    # memory units
    "b": 1,
    "kb": 2 ** 10,
    "mb": 2 ** 20,
    "gb": 2 ** 30,
    # time units
    "s": 1,
    "m": 60,
    "h": 60 * 60,
    "d": 24 * 60 * 60,
}

RE_VALUE = re.compile(
    rf"^(?P<amount>\d+)\s*(?P<unit>{'|'.join(SUPPORTED_UNITS.keys())})"
)
