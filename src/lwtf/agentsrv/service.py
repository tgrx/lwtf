import logging
import os
import sqlite3
import subprocess
import tempfile
from contextlib import closing
from datetime import datetime
from pathlib import Path
from typing import Text

from lwtf.core.utils import get_pipenv_path, get_venv_path

L = logging.getLogger("agentsrv")


class AgentSrvService:
    def __init__(self, host="localhost", port=80, schema="http", db=None):
        L.debug(
            f"init agentsrv with host=`{host!r}` port=`{port!r}` schema=`{schema!r}` db=`{db!r}`"
        )
        self.schema = schema
        self.host = host
        self.port = port
        self.db_original = db
        self.db = None
        self.srv = None
        self.env = None
        L.debug(
            f"ok: init with host=`{host!r}` port=`{port!r}` schema=`{schema!r}` db=`{db!r}`"
        )

    def __del__(self):
        L.debug(f"del agentsrv")
        self.shutdown()
        self.srv = self.env = self.db_original = self.db = None
        L.debug(f"ok: del agentsrv")

    def start(self):
        L.debug(f"start agentsrv")
        self.do_self_health_check()
        self.init_db()
        self.start_wsgi()
        L.debug(f"ok: start agentsrv")

    def shutdown(self):
        L.debug(f"shotdown agentsrv")
        self.shutdown_wsgi()
        L.debug(f"ok: shotdown agentsrv")

    @property
    def url(self) -> Text:
        self.do_self_health_check()

        return f"{self.schema}://{self.host}:{self.port}/agent"

    def do_self_health_check(self):
        L.debug(f"healthcheck agentsrv")
        assert self.schema in {"http", "https"}, "malformed schema"
        assert self.host, "undefined host"
        assert self.port, "undefined port"
        L.debug(f"ok: healthcheck agentsrv")

    def init_db(self):
        L.debug(f"init db agentsrv")
        self.db = SqliteDW(self.db_original)
        L.debug(f"ok: init db agentsrv")

    def start_wsgi(self):
        L.debug(f"start wsgi agentsrv")
        self._setup_env()
        self._start_gunicorn_subprocess()
        L.debug(f"ok: start wsgi agentsrv")

    def shutdown_wsgi(self):
        L.debug(f"shutdown wsgi agentsrv")
        self._terminate_gunicorn_subprocess()
        L.debug(f"ok: shutdown wsgi agentsrv")

    def _setup_env(self):
        L.debug(f"setup env agentsrv, db url=`{self.db.url!r}`")
        self.env = os.environ.copy()
        self.env["AGENTSRV_DB_URL"] = self.db.url
        L.debug(f"ok: setup env agentsrv")

    def _start_gunicorn_subprocess(self):
        L.debug(f"start gunicorn subprocess agentsrv")

        from lwtf.agentsrv import wsgi

        args = (
            f"{get_pipenv_path().as_posix()}",
            "run",
            "gunicorn",
            "--workers=2",
            f"--bind=0.0.0.0:{self.port}",
            f"--pythonpath='{get_venv_path().as_posix()}'",
            "--access-logfile=-",
            f"{wsgi.__name__}:app",
        )

        L.debug(f"args: `{' '.join(args)!r}`")

        L.debug(f"create a subprocess")
        self.srv = subprocess.Popen(args=args, env=self.env)
        L.debug(f"done: create a subprocess! srv=`{self.srv}`, pid=`{self.srv.pid}`")

    def _terminate_gunicorn_subprocess(self):
        L.debug(f"terminate ginicorn subprocess")
        try:
            L.debug(f"terminate")
            self.srv.terminate()
            L.debug(f"done: terminate!")

            L.debug(f"wait")
            self.srv.wait(5)
            L.debug(f"done: wait!")
        except subprocess.TimeoutExpired:
            L.debug(f"oops! timeout, send KILL")
            self.srv.kill()
            L.debug(f"done: send KILL!")

        L.debug(f"done: terminate ginicorn subprocess!")


class SqliteDW:
    def __init__(self, db=None):
        self.db = db

    @property
    def url(self):
        self._init()
        return self.db.name

    def _init(self):
        self._init_db_file()
        self._verify_db_file_existence()
        self._create_tables()

    def _init_db_file(self):
        if self.db:
            return

        ts = datetime.now().strftime("%Y%m%d_%H%M%S%f")
        self.db = tempfile.NamedTemporaryFile(
            prefix=f"agentsrv_{ts}_", suffix=".sqlite"
        )

    def _verify_db_file_existence(self):
        assert self.db, "db is not set"
        assert Path(self.db.name).is_file(), f"cannot access the db at `{self.db.name}`"

    def _create_tables(self):
        q = """
            CREATE TABLE
            IF NOT EXISTS
            agents(
                ts DATETIME DEFAULT CURRENT_TIMESTAMP,
                agent TEXT,
                addr TEXT,
                osdata TEXT
            );
            """
        self._execute_query(q)

    def _execute_query(self, q):
        with closing(
            sqlite3.connect(database=self.db.name, isolation_level=None)
        ) as conn:
            return conn.execute(q).fetchall()
