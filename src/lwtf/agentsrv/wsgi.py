import json
import os
import sqlite3
from contextlib import closing, contextmanager
from typing import Text

import falcon
from falcon import Request, Response

from lwtf.core.project_logging import configure_logging


def get_db_url() -> Text:
    url = os.getenv("AGENTSRV_DB_URL")
    assert url
    return url


def get_db_connection():
    url = get_db_url()
    conn = sqlite3.connect(database=url, isolation_level=None)
    return conn


@contextmanager
def DB():
    with closing(get_db_connection()) as conn:
        yield conn


def save_to_db(agent, addr, osdata):
    q = """
        INSERT INTO agents(agent, addr, osdata)
        VALUES
        (?, ?, ?);
        """

    with DB() as db:
        db.execute(q, (agent, addr, osdata))


def fetch_from_db(agent=None):
    fields = sorted({"addr", "agent", "osdata", "ts"})

    q, v = f"SELECT {','.join(fields)} FROM agents", ()
    if agent:
        q += " WHERE agent=?"
        v = (agent,)

    q += " ORDER BY ts DESC, agent ASC"
    q += ";"

    with DB() as db:
        resp = db.execute(q, v).fetchall()

    return [dict(zip(fields, row)) for row in resp]


class AgentResource:
    def on_get(self, _req, resp, agent=None):
        payload = json.dumps(fetch_from_db(agent), sort_keys=True, indent=2)

        resp.body = payload

    def on_post(self, req: Request, resp: Response):
        payload = json.loads(req.stream.read())

        addr = req.remote_addr
        agent = payload["agent"]
        osdata = payload["osdata"]

        save_to_db(agent, addr, osdata)


configure_logging()

app = falcon.API()
app.add_route("/agent", AgentResource())
app.add_route("/agent/{agent}", AgentResource())
