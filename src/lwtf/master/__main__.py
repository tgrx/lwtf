import logging
from typing import Dict

import docker

from lwtf.agentsrv.service import AgentSrvService
from lwtf.core.project_logging import configure_logging
from lwtf.core.project_typing import DockerT, TestCaseT
from lwtf.master.actions import (
    test_action__check_clean_system,
    test_action__install_agent,
    test_action__send_os_data,
    test_action__uninstall_agent,
)

L = logging.getLogger("master")


def start_services(context: Dict):
    L.debug("start services")

    srv = AgentSrvService(host="host.docker.internal", port=8000)
    context["agentsrv"] = srv
    srv.start()

    L.info(f"done: start services")


def shutdown_services(context: Dict):
    L.debug("shutdown services")

    srv = context.get("agentsrv")
    if not isinstance(srv, AgentSrvService):
        L.debug("no agentsrv running, skip")
        return

    srv.shutdown()
    L.info(f"done: shutdown services")


def setup_docker(platform: DockerT):
    L.debug(f"create client")
    client = docker.from_env()

    L.debug(f"pull image: `{platform.image}`")
    image = client.images.pull(platform.image)

    L.debug(f"create container with command: `{platform.command}`")
    container = client.containers.create(image=image, command=platform.command)

    L.debug(f"start container")
    container.start()

    L.info(f"done: start docker container `{container.name}` on `{platform.image}`")
    return container


def setup_envs(context: Dict, test: TestCaseT):
    L.debug("setup envs")

    for platform in test.platforms:
        L.debug(f"set up platform: `{platform}`")

        if isinstance(platform, DockerT):
            L.debug(f"platform type is known: Docker, create container")
            container = setup_docker(platform)

            context.setdefault("containers", []).append(container)
            L.debug(f"container `{container.name}` is saved in test context")

    L.info("done: setup envs")


def shutdown_envs(context: Dict) -> None:
    L.debug("shutdown envs")

    for container in context.get("containers", []):
        L.debug(f"tear down container `{container.name}`")
        try:
            L.debug(f"kill `{container.name}`")
            container.kill()
        finally:
            try:
                L.debug(f"stop `{container.name}`")
                container.stop()
            finally:
                L.debug(f"remove `{container.name}`")
                container.remove(v=True)

        L.info(f"container `{container.name}` is removed")

        # TODO: may be redundand
        L.debug(f"pruning stopped containers")
        client = docker.from_env()
        r = client.containers.prune()
        L.info(f"stopped containers are pruned")

    L.info("done: shutdown envs")


def exec_actions(context: Dict, test: TestCaseT):
    L.debug(f"execute actions of test: `{test.name}`")

    actions_map = {
        "check clean system": test_action__check_clean_system,
        "install agent": test_action__install_agent,
        "send os data": test_action__send_os_data,
        "uninstall agent": test_action__uninstall_agent,
    }

    for container in context.get("containers", []):
        L.info(f"testing on container: `{container.name}` @ {container.image}")

        for action in test.actions:
            action_func = actions_map[action]
            action_func(context, container)
            L.info(f"perform action `{action}`: ok")

    L.info("done: exec actions")


def run_test(test: TestCaseT) -> None:
    L.debug(f"run test: `{test.name}`")

    ctx = {"test": test}

    try:
        start_services(ctx)
        setup_envs(ctx, test)
        exec_actions(ctx, test)
    except Exception as exc:
        L.warning(f"test `{test.name}` failed: {exc!r}")
        raise
    else:
        L.info(f"test `{test.name}` passed")
    finally:
        shutdown_envs(ctx)
        shutdown_services(ctx)


def run():
    L.info("testing started")

    for test in TestCaseT.list_tests():
        L.info(f"test found at {test.path}")
        run_test(test)


if __name__ == "__main__":
    configure_logging()
    run()
