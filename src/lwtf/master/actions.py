import json
import logging
import os
from datetime import datetime
from typing import Dict

L = logging.getLogger("actions")


def test_action__check_clean_system(context: Dict, container):
    L.debug(f"check that agent is not installed")
    cmd = container.exec_run("curl --version")
    assert cmd.exit_code != 0
    assert b"curl" in cmd.output
    assert b"not found" in cmd.output

    L.debug("ok")


def test_action__install_agent(context: Dict, container):
    install_commands = {
        "ubuntu": ["apt-get --yes update", "apt-get --yes install curl"],
        "alpine": ["apk add curl"],
    }

    cmd_set = None
    for _os_name, _cmd in install_commands.items():
        for _tag in container.image.tags:
            if _os_name in _tag:
                cmd_set = _cmd
                break
        if cmd_set:
            break

    L.debug(f"installing agent with cmd set: `{cmd_set}`")
    for cmd in cmd_set:
        L.debug(f"executing command `{cmd}`")
        ret = container.exec_run(cmd)
        L.debug(f"command `{cmd}` exited with {ret.exit_code}: `{ret.output!s}`")
        assert ret.exit_code == 0

    L.debug(f"check that agent is installed")
    ret = container.exec_run("curl --version")
    assert ret.exit_code == 0
    assert b"curl" in ret.output

    L.debug("ok")


def test_action__send_os_data(context: Dict, container):
    L.debug(f"sending os data")
    agent = f"agent_{os.urandom(8).hex()}"
    data_sent = {"agent": agent, "osdata": "kek"}
    ts_sent = datetime.now()

    cmd = container.exec_run(
        f"curl -XPOST {context['agentsrv'].url} -d '{json.dumps(data_sent)}'"
    )
    assert cmd.exit_code == 0

    L.debug(f"check sent os data")
    cmd = container.exec_run(f"curl --silent -XGET {context['agentsrv'].url}/{agent}")
    assert cmd.exit_code == 0

    data_recv = json.loads(cmd.output.decode())
    L.debug(f"os data: `{data_recv}`")

    assert len(data_recv) == 1

    data_recv = data_recv[0]
    assert data_recv["agent"] == data_sent["agent"]
    assert data_recv["osdata"] == data_sent["osdata"]
    assert data_recv["addr"] == "127.0.0.1"

    ts_recv = datetime.fromisoformat(data_recv["ts"])
    assert (ts_recv - ts_sent).total_seconds() <= 60

    L.debug("ok")


def test_action__uninstall_agent(context: Dict, container):
    install_commands = {
        "ubuntu": ["apt-get --yes remove curl"],
        "alpine": ["apk del curl"],
    }

    cmd_set = None
    for _os_name, _cmd in install_commands.items():
        for _tag in container.image.tags:
            if _os_name in _tag:
                cmd_set = _cmd
                break
        if cmd_set:
            break

    L.debug(f"uninstalling agent with cmd set: `{cmd_set}`")
    for cmd in cmd_set:
        L.debug(f"executing command `{cmd}`")
        ret = container.exec_run(cmd)
        L.debug(f"command `{cmd}` exited with {ret.exit_code}: `{ret.output!s}`")
        assert ret.exit_code == 0

    L.debug(f"ok")
