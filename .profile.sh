#!/usr/bin/env bash

BASE_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
export BASE_DIR

export PYTHONPATH=$BASE_DIR/src:$PYTHONPATH

# TODO: if MacOsX
#export OBJC_DISABLE_INITIALIZE_FORK_SAFETY=YES

cd "$BASE_DIR" || exit 1
